const http = require('http');
const fs = require('fs');
const url = require('url');
const logsPath = './logs/logs.json';

module.exports = () => {
    http.createServer(function (req, res) {
        const {pathname, query} = url.parse(req.url, true);
        if (!fs.existsSync('./file')) {
            fs.mkdirSync('./file');
        }
        if (!fs.existsSync('./logs')) {
            fs.mkdirSync('./logs');
        }
        if (req.method === 'POST') {
            if (pathname === "/file") {
                if (query.filename && query.content) {
                    fs.writeFile(`./file/${query.filename}`, query.content, (err) => {
                        if (err) {
                            throw err;
                        }
                        createLogs(`New file with name ‘${query.filename}’ saved`);
                        res.writeHead(200, {'Content-type': 'application/json'});
                        res.end();
                    })
                } else {
                    res.writeHead(400, {'Content-type': 'text/html'});
                    res.end(`Missing query`);
                }
            } else {
                res.writeHead(404, {'Content-type': 'text/html'});
                res.end(`Missing resource`);
            }
        } else if (req.method === 'GET') {
            if (pathname.includes("/file/")) {
                fs.readFile(`.${pathname}`, 'utf-8', (err, content) => {
                    if (err) {
                        const message = `No files with name: '${pathname.replace("/file/", "")}' found`;
                        res.writeHead(400, {'Content-type': 'text/html'});
                        res.end(message);
                        return;
                    }
                    createLogs(`File with name ‘${pathname.replace("/file/", "")}’ read`);
                    res.writeHead(200, {'Content-type': 'application/json'});
                    res.end(content);
                })
            } else if (pathname === "/logs") {
                checkIfLogsExist();
                fs.readFile(logsPath, 'utf-8', (err, content) => {
                    if (err) {
                        res.writeHead(400, {'Content-type': 'text/html'});
                        res.end(`No files by path: "${pathname}" found`);
                        return;
                    } else if (query.from && query.to) {
                        const {logs} = JSON.parse(content);
                        const result = logs.filter((log) => query.to >= log.time && log.time >= query.from);
                        res.writeHead(200, {'Content-type': 'application/json'});
                        res.end(JSON.stringify({logs:result}, null, 3));
                    } else {
                        const {logs} = JSON.parse(content);
                        res.writeHead(200, {'Content-type': 'application/json'});
                        res.end(JSON.stringify({logs:logs}, null, 3));
                    }
                })
            } else {
                res.writeHead(404, {'Content-type': 'text/html'});
                res.end(`No resource by path: "${pathname}" found`);
            }
        } else {
            res.writeHead(405, {'Content-type': 'text/html'});
            res.end('Method Not Allowed');
        }
    }).listen(process.env.PORT || 8080);
}

function createLogs(message) {
    checkIfLogsExist();
    fs.readFile(logsPath, 'utf-8', (err, content) => {
        if (err) {
            throw err;
        }
        const {logs} = JSON.parse(content);
        logs.push({message: message, time: Date.now()});
        const data = JSON.stringify({logs}, null, 3);
        fs.writeFile(logsPath, data, (err) => {
            if (err) {
                throw err;
            }
        })
    })
}

function checkIfLogsExist() {
    //console.log('Checking Logs');
    if (!fs.existsSync(logsPath)) {
        fs.writeFileSync(logsPath, '{"logs":[]}');
        //console.log('Logs was created');
    } else {
        //console.log('Logs exist');
    }
}